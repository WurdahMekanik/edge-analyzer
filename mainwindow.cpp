#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtWidgets>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _sobelKernel(-1),
    _laplacianKernel(1)
{
    ui->setupUi(this);

    ui->imgBefore->setScaledContents(true);
    ui->imgAfter->setScaledContents(true);

    _fileDialog = new QFileDialog(this, tr("Open Image"), tr("/home/manager/Pictures"));
    _fileDialog->setMimeTypeFilters({"image/jpeg", "image/png", "image/bmp", "image/tiff",
                                     "image/x-portable-bitmap", "image/x-portable-graymap",
                                     "image/x-portable-anymap", "image/x-portable-pixmap"});

    connect(ui->actionOpen_Image, &QAction::triggered, [this]{
        QString filename = _fileDialog->getOpenFileName();
        _img = cv::imread(filename.toStdString());
        if (_img.empty()) {
            QMessageBox::warning(this, "No Image", "No image loaded, or image file empty.");
            _imgLoaded = false;
        } else {
            _imgLoaded = true;
            blurImage();
        }
    });

    static void (QSpinBox::*spinValueChanged)(int) = &QSpinBox::valueChanged;
    connect(ui->blurSize, spinValueChanged, this, &MainWindow::blurImage);
    connect(ui->calcSobel, &QPushButton::clicked, this, &MainWindow::calcSobel);
    connect(ui->sobelKernel, spinValueChanged, [this](int v){ _sobelKernel = v; });
    connect(ui->calcLaplacian, &QPushButton::clicked, this, &MainWindow::calcLaplacian);
    connect(ui->laplacianKernel, spinValueChanged, [this](int v){ _laplacianKernel = v; });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::blurImage()
{
    if (!_imgLoaded) {
        QMessageBox::warning(this, "No Image", "No image loaded.");
        return;
    }

    // Blur image using Gaussian filter
    int blurSize = ui->blurSize->value();
    GaussianBlur(_img, _imgBefore, cv::Size(blurSize, blurSize), 0, 0, cv::BORDER_DEFAULT);

    // Update QLabel
    QImage qimg(_imgBefore.data, _imgBefore.cols, _imgBefore.rows, static_cast<int>(_imgBefore.step), QImage::Format_RGB888);
    // Image display scaled down to prevent giant iamges from filling the screen.
    // TODO: provide more intelligent scaling.
    ui->imgBefore->setPixmap(QPixmap::fromImage(qimg.rgbSwapped()).scaled(_imgBefore.cols/3, _imgBefore.rows/3, Qt::KeepAspectRatio));
}

void MainWindow::calcSobel()
{
    if (!_imgLoaded) {
        QMessageBox::warning(this, "No Image", "No image loaded.");
        return;
    }

    // Calculate Sobel derivative
    cv::Mat grad_x, grad_y;
    cv::Sobel(_imgBefore, grad_x, CV_8U, 1, 0, _sobelKernel);
    cv::Sobel(_imgBefore, grad_y, CV_8U, 0, 1, _sobelKernel);

    // Convert back to BGR
    cv::Mat abs_grad_x, abs_grad_y;
    convertScaleAbs(grad_x, abs_grad_x);
    convertScaleAbs(grad_y, abs_grad_y);
    cv::Mat dst;
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst);

    // Update QLabel
    QImage qimg2(dst.data, dst.cols, dst.rows, static_cast<int>(dst.step), QImage::Format_RGB888);
    // Image display scaled down to prevent giant iamges from filling the screen.
    // TODO: provide more intelligent scaling.
    ui->imgAfter->setPixmap(QPixmap::fromImage(qimg2.rgbSwapped()).scaled(dst.cols/3, dst.rows/3, Qt::KeepAspectRatio));
}

void MainWindow::calcLaplacian()
{
    if (!_imgLoaded) {
        QMessageBox::warning(this, "No Image", "No image loaded.");
        return;
    }

    // Calculate laplacian
    cv::Mat dst;
    cv::Laplacian(_imgBefore, dst, CV_16U, _laplacianKernel);

    // Convert back to BGR
    cv::Mat abs_dst;
    convertScaleAbs(dst, abs_dst);

    // Update QLabel
    QImage qimg2(abs_dst.data, abs_dst.cols, abs_dst.rows, static_cast<int>(abs_dst.step), QImage::Format_RGB888);
    // Image display scaled down to prevent giant iamges from filling the screen.
    // TODO: provide more intelligent scaling.
    ui->imgAfter->setPixmap(QPixmap::fromImage(qimg2.rgbSwapped()).scaled(abs_dst.cols/3, abs_dst.rows/3, Qt::KeepAspectRatio));
}
