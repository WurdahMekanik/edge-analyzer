#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QPointer>

#include "opencv2/opencv.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void blurImage();
    void calcSobel();
    void calcLaplacian();

private:
    Ui::MainWindow *ui;
    QPointer<QFileDialog> _fileDialog;

    bool _imgLoaded;
    cv::Mat _img;
    cv::Mat _imgBefore;

    int _sobelKernel;
    int _laplacianKernel;
};

#endif // MAINWINDOW_H
